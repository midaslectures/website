// twitter
window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));

// google
(function(i,s,o,g,r,a,m){
	i['GoogleAnalyticsObject']=r;
		i[r]=i[r]||function(){
  			(i[r].q=i[r].q||[]).push(arguments)
  		},i[r].l=1*new Date();
  		a=s.createElement(o),m=s.getElementsByTagName(o)[0];
  		a.async=1;
  		a.src=g;
  		m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-63335516-1', 'auto');
ga('send', 'pageview');

// custom
function w3_open() {
    document.getElementById("mySidenav").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
function w3_close() {
    document.getElementById("mySidenav").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}

$(function(){
    $("#headerDiv").load("commonHTML/headerDiv.html"); 
    $("#topDiv").load("commonHTML/topDiv.html"); 
    $("#sideNavDiv").load("commonHTML/sideNavDiv.html");
    $("#footerDiv").load("commonHTML/footerDiv.html"); 
});
